# Installing Docker

You need Docker Engine and Docker Compose. These are both included with Docker Desktop. Please follow the instructions linked for your operating system.


OS | Product | Link to install instructions
-- | ------- | ----------------------------
Windows 10 Home | Docker Desktop | https://docs.docker.com/docker-for-windows/install-windows-home/
Windows 10 Education <br> Windows 10 Professional <br> Windows 10 Enterprise | Docker Desktop | https://docs.docker.com/docker-for-windows/install/
MacOS | Docker Desktop | https://docs.docker.com/docker-for-mac/install/
Linux | Docker Engine AND <br> Docker Compose | https://docs.docker.com/engine/install/ <br> https://docs.docker.com/compose/install/

## Testing your installation

Make sure Docker Desktop is running (see installation instructions above). Then open a terminal and try running `docker --version` and `docker-compose --version`. You should get output similar to the following.

```bash
❯ docker --version
Docker version 19.03.12, build 48a66213fe
❯ docker-compose --version
docker-compose version 1.26.2, build eefe0d31
❯ 
```

Next, test Docker a little more thoroughly by running the `hello-world` container.

```bash
❯ docker run --rm hello-world
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:7f0a9f93b4aa3022c3a4c147a449bf11e0941a1fd0bf4a8e6c9408b2600777c5
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

❯ 
```

Last, test Docker Compose a little more thoroughly by creating a file named `docker-compose.yml` with the following contents.

```bash
version: "3.8"
services:
  hello-world:
    image: hello-world
```

And then in the same directory as that file, run `docker-compose up`. You should see something like the following.

```bash
❯ docker-compose run --rm hello-world
Creating network "activities_default" with the default driver

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/

❯
```

Last, let's cleanup the network created by the last command by running `docker-compose down`.

```bash
❯ docker-compose down
Removing network activities_default
❯
```

---

&copy; 2020 Stoney Jackson <dr.stoney@gmail.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
