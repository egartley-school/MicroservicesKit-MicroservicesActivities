# Designing REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

## Model 1: OpenAPI Specification

> The OpenAPI Specification (OAS) defines a standard, programming language-agnostic interface description for REST APIs, which allows both humans and computers to discover and understand the capabilities of a service without requiring access to source code, additional documentation, or inspection of network traffic. When properly defined via OpenAPI, a consumer can understand and interact with the remote service with a minimal amount of implementation logic. Similar to what interface descriptions have done for lower-level programming, the OpenAPI Specification removes guesswork in calling a service.
&mdash; [OpenAPI Specification V3.0.3](http://spec.openapis.org/oas/v3.0.3)

The API for the OrderModule that you have been exploring was specified using the OpenAPI specification.

Open `openapi.yml` in either:
- Your cloned copy of [microservices-example](https://gitlab.com/LibreFoodPantry/training/microservices-example) in the `OrderModule` directory
- On [GitLab](https://gitlab.com/LibreFoodPantry/training/microservices-example/-/blob/master/OrderModule/openapi.yml) and choose `Display source` (the `</>` button)

This is a [YAML](https://en.wikipedia.org/wiki/YAML) file. You should be (somewhat) familiar with the format - this is the same format that is used for `docker-compose` files. YAML is a superset of JSON.

This `openapi.yml` is translated by the `openapi-generator` program into the HTML files with the API's documentation that you explored in activities 05 and 06. It can also be translated into skeleton code for servers, clients, documentation, schemas, and configurations to handle the REST API calls. `openapi-generator` has [generators](https://openapi-generator.tech/docs/generators/) for many languages, but the `microservices-example` it produce server code for `nodejs-express`. (We will look at that code in activity 11.)

### Questions
Answer the following questions while looking at the `openapi.yml` file:

1. What is the purpose of the `tags:` section? How do the tags relate to the structure of our architecture?
2. How many `paths:` are there? How does that compare to the number of endpoints you saw in the documentation? Is it the same or different? Look again at your answers in activity 05 or view the web documentation (either from your running server, or by clicking on the `Display rendered file` in GitLab.)
3. How many methods appear under the `/orders` path? Does this explain the difference in your answers to question 2?
4. Compare the `parameters:` in `/items` to the documentation.
5. What is `$ref` used for? Why is this a good idea?
6. Compare `requestBody:` in `/orders` to the documentation.
7. Compare `responses:` in `/orders/{id}` to the documentation.
8. Does `operationID` appear in the documentation? Can you guess what it is used for?
9. Look at the rest of the file, and note any other interesting things you find.

## Additional OpenAPI Resources

- [OpenAPI Map](https://openapi-map.apihandyman.io/) at [API Handyman](https://apihandyman.io/)
- [OpenAPI Generator](https://openapi-generator.tech/)
- [The Design of Web APIs](https://learning.oreilly.com/library/view/the-design-of/9781617295102/) by Arnaud Lauret, Manning Publications, October 2019, 9781617295102

---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.