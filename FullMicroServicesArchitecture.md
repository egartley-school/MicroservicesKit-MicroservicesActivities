```plantuml
@startuml
actor User

rectangle WebBrowser {

   package "OrderFrontEndWebPage" {
      [ItemsDisplay]
      [OrdersDisplay]
      [OrdersApprovalDisplay]
   }
}

rectangle OrderModule {
   package "OrderFrontEnd" {
      interface port80
      port80 -- [OrderFrontEndWebServer]
   }

   package "PlaceOrderAPI" {
      interface getItems
      interface getOrders
      interface postOrders
      interface getOrdersById
      getItems -- [PlaceOrderService]
      getOrders -- [PlaceOrderService]
      postOrders -- [PlaceOrderService]
      getOrdersById -- [PlaceOrderService]
   }

   package "ApproveOrderAPI" {
      interface getOrdersApproved
      interface getOrdersUnreviewed
      interface patchOrdersIdApprove
      interface patchOrdersIdCancel
      getOrdersApproved -- [ApproveOrderService]
      getOrdersUnreviewed -- [ApproveOrderService]
      patchOrdersIdApprove -- [ApproveOrderService]
      patchOrdersIdCancel -- [ApproveOrderService]
   }

   package "PlaceOrderDB" {
       Database PlaceOrderDatabase
   }

   package "ApproveOrderDB" {
      Database ApproveOrderDatabase
   }

   OrderFrontEndWebPage <--> port80 : "Request/Fetch Page"
   [ItemsDisplay] <--> getItems
   [OrdersDisplay] <--> getOrders
   [OrdersDisplay] <--> getOrdersById
   [OrdersDisplay] <--> postOrders
   [OrdersApprovalDisplay] <--> getOrdersApproved
   [OrdersApprovalDisplay] <--> getOrdersUnreviewed
   [OrdersApprovalDisplay] <--> patchOrdersIdApprove
   [OrdersApprovalDisplay] <--> patchOrdersIdCancel
   [ApproveOrderService] <--> ApproveOrderDatabase
   [PlaceOrderService] <--> PlaceOrderDatabase
   User <-> WebBrowser
}
@enduml
```