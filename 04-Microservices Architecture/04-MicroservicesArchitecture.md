# Software Architectures from Monolith to Microservices

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

# Software Architectures

## Monolith
```plantuml
@startuml
actor Admin
actor Guest

node AdminBrowser
node GuestBrowser
node OrderWebService <<server>>

Database Database

Guest -> GuestBrowser
AdminBrowser <- Admin
OrderWebService <- AdminBrowser
GuestBrowser -> OrderWebService
OrderWebService <--> Database
@enduml
```

## Microservices Architecture
```plantuml
@startuml
Actor Admin
Actor Guest

node "GuestBrowser" {
  [PlaceOrderUi]
}

node "AdminBrowser" {
  [ApproveOrderUi]
}

package "ApproveOrderService" {
  node ApproveOrderWeb <<Server>> {
    [ApproveOrderUi] as aoui
  }
  node ApproveOrderApi <<Server>>
  database ApproveOrderDb <<Server>>
}

package "PlaceOrderService" {
  node PlaceOrderWeb <<Server>> {
    [PlaceOrderUi] as poui
  }
  node PlaceOrderApi <<Server>>
  database PlaceOrderDb <<Server>>
}

Admin -> ApproveOrderUi
aoui ..> ApproveOrderUi
ApproveOrderUi -> ApproveOrderApi
ApproveOrderApi -> ApproveOrderDb
ApproveOrderApi -> PlaceOrderApi

Guest -> PlaceOrderUi
poui ..> PlaceOrderUi
PlaceOrderUi -> PlaceOrderApi
PlaceOrderApi -> PlaceOrderDb
@enduml
```

1. How many databases does the monolith have?
2. How many databases does the microservices have?
3. If the database became corrupted or unavailable in the monolith, what parts of the monolith would be impacted?
4. If the PlaceOrder Database became corrupted or unavaiable what parts of the microservice system would be impacted?
5. Suppose this system is being maintained by 3 development teams (one for each service) and one operations team (responsible for ensuring the integrity, security, stability, availability, etc. of the system in production). You are on the Approve Orders team. You want to update the schema for the database to improve the functionality of the Approve Orders service.
   1. Under a monolith architecture, how easy do you think it will be to update the database and get the new database into production and why?
   2. Under a microservice architecture, assuming the change needs to be made to the Place Order database, how easy do you think it will be to make the change and get it into production and why?
6. Under which architecture would it be easier to update components and why?
7. Under which architecture would it be safer to update components and why?
8. Under which architecture would the operations team rather delay updates for as long as possible and why?

---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
