# Microservices example for LibreFoodPantry onboarding

Provides materials that can be used as in-class activities or onboarding tutorials for students or independent developers learning to work within the microservices architecture that is being used by LibreFoodPantry.

LibreFoodPanty has adopted the microservices architecture because it makes it easier for individual classes or student teams to

- work on smaller, easier to understand modules
- develop code in whatever language or with whatever tools they are already comfortable
- redesign/redevelop modules to meet the needs of their own campus’ food pantry
- work on frontend or backend code based on interest or course goals

But most students and faculty are unfamiliar with the microservices architecture, and so learning materials are needed to get them up-to-speed on this style of development.

The set of POGIL activities to be developed will teach students to design a REST API, implement the REST API as a backend server with a persistence layer using a document database, develop and implement a frontend that interacts with the user and calls the backend through the REST API.

The activities will be developed around a simplified version of Bear Necessities Market’s ordering system. Bear Necessities Market (BNM) is a member project of LibreFoodPantry. The example ordering system will use a smaller set of features than BNM – just enough to support the learning goals of the activities, while still giving the feel of the larger system.

The working code examples given for the students to explore, modify, and extend will be containerized in the same way that LFP services are being containerized in Docker.

The POGIL activities are in the [Activities](Activities) directory.