# Exploring REST API Calls

## Content Learning Objectives


_After completing this activity, students should be able to:_

*

## Process Skill Goals

_During the activity, students should make progress toward:_

*

## Team Roles

_Decide what role each of you will play for today. Choose a role that you have not played before, or recently. The goal should be to have all team members rotate through the roles on a regular basis to become comfortable with all the roles. If you have only three people, one should have two roles. If you have five people, two may share the same role. Record role assignments here._


<table>
  <tr>
   <td>
Manager
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Presenter
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Recorder
   </td>
   <td>
   </td>
  </tr>
  <tr>
   <td>Technician/
<br>
Reflector
   </td>
   <td>
   </td>
  </tr>
</table>

## Model 1: Using the Order System REST API

Have your technician do the following:

### Get and Start the Microservices Example Project

1. Clone the [Microservices Example project](https://gitlab.com/LibreFoodPantry/training/microservices-example) from LibreFoodPantry.
2. Open the README.md file
   1. Follow the steps under `Setup`. This will involve installing the following (if you do not already have them installed):
      - Bash
      - Git
      - Docker Desktop
   Note that there are install instructions for both MacOS and Windows OS computers.
3. Open a terminal/command window inside the top-level directory of the cloned project.
4. Build and run the development shell. (This will need to be down before executing any of the `make` commands):

   ```bash
   shell/run.sh
   ```

3. Start the system with the command:

   ```bash
   make build up
   ```

### Explore the REST API using the Web Documentation

- The API documentation for the Place Order feature: [http://localhost:10001/api-docs/](http://localhost:10001/api-docs/)

   - Remember that this is for Place Order. Collapse the Approve Order feature.

- The API documentation for the Approve Order feature: [http://localhost:10011/api-docs/](http://localhost:10011/api-docs/)

   - Remember that is is for Approve Order. Collapse the Place Order feature.

### Questions
In Place Order:
1. Try out the `GET /items` endpoint. Click the `Try it out` button, then click the `Execute` button. What changed?
2. Open your browser, paste in the `Request URL`, click enter. What happened?
3. Try out the `GET /orders` endpoint. What did you get? Explain the response.
4. Try out the `PUT /orders` endpoint:

    -  You will need to put values in for `preferences`, `restrictions`, and `email`.
    -  You will need to add items to the `itemsList`. I suggest going back up to the `GET \items` response, and copy and paste items from there. Before doing so, notice the example format in `itemsList` - in particular where you should, and should not, include commas.
    - Paste the `Request body` you created here.
    - Why is there no `Request URL`?
    - Explain the response.
6. Try out the `GET /orders/{id}` endpoint. Use the `_id` from the order you created in Question 4.

In Approve Order:

5. Try out `GET /orders/approved`. Explain the response, in particular the contents of the `orders` list.
6. Try out `GET /orders/unreviewed`. Explain the response, in particular the contents of the `orders` list.
7. Create another order as you did in Question 4. Make sure it differs enough from your previous order so that you can tell the difference.
8. Call `GET /orders/unreviewed` again.
9. Try out `PATCH /orders/{id}/approve`. Choose one of the two orders you created to approve.
10. Try out `GET /orders/approved` and `GET /orders/unreviewed` again. Explain the results.
11. 9. Try out `PATCH /orders/{id}/cancel`. Choose the other order you created to cancel.
12. Try out `GET /orders/approved` and `GET /orders/unreviewed` again. Explain the results.
13. What endpoint is "missing" form the API, that you could see a use for? Explain why you would want to have it.

### Explore the REST API using a REST API Tool

Have your technician (at least<sup>*</sup>) install a REST API Tool. Either of the following is fine. Both are free to use, but Insomnia is Open Source, Postman is not.

  - Insomnia Core ([https://insomnia.rest/](https://insomnia.rest/))
  - Postman ([https://www.getpostman.com/](https://www.getpostman.com/)).

<sup>*</sup>You will all probably want to install one of these tools, but you can do that outside of class.

### Questions

14. Continue exploring the API using the REST API tool. You can repeat some of the questions from above. Your data from Questions 1-13 will still be there.

*   The base URL for Place Order is [http://localhost:10001](http://localhost:10001)
*   The base URL for Approve Order is [http://localhost:10011](http://localhost:10011)

## Stop the Microservices Example Project

When finished, stop the system with the command:

   ```bash
   make down
   ```

---

&copy; 2020 Karl R. Wurst <karl@w-sts.com>

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/]() or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
